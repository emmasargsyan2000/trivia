import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import {ResultDetailComponent} from "./result-detail/result-detail.component";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {ResultService} from "./services/result.service";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [
    DashboardComponent,
    ResultDetailComponent
  ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule
    ],
  providers: [
    ResultService
  ]
})
export class DashboardModule { }
