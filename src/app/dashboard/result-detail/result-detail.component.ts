import {Component, inject, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs";
import {TestResult} from "../../quiz/models/question";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-result-detail',
  templateUrl: './result-detail.component.html',
  styleUrls: ['./result-detail.component.scss']
})
export class ResultDetailComponent implements OnInit {

  public testResult!: TestResult;
  public displayedColumns: string[] = ['question', 'difficulty', 'correct_answer', 'selected_answer', 'is_correct'];
  private router: Router = inject(Router);
  private route: ActivatedRoute = inject(ActivatedRoute);

  ngOnInit() {
    this.route.data.pipe(first()).subscribe(( {testResult} ) => {
      this.testResult = testResult;
    });
  }

  back(){
    this.router.navigate(['../'], {relativeTo: this.route});
  }

}
