import {Component, inject, OnInit} from '@angular/core';
import {ResultService} from "../services/result.service";
import {TestResult} from "../../quiz/models/question";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public testResults: TestResult[] = []
  public displayedColumns: string[] = ['index', 'category', 'scored', 'total'];
  private router: Router = inject(Router);
  private route: ActivatedRoute = inject(ActivatedRoute);
  private resultService: ResultService = inject(ResultService);

  ngOnInit(): void {
    this.testResults = this.resultService.getTestsResult()
  }

  public onClick(test: TestResult){
    this.router.navigate(['./' + test.index], {relativeTo: this.route});
  }

}
