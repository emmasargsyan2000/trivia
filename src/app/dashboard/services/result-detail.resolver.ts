import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from "@angular/router";
import {inject} from "@angular/core";
import {TestResult} from "../../quiz/models/question";
import {ResultService} from "./result.service";

export const testResolver: ResolveFn<TestResult | undefined> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
    return inject(ResultService).getTestResult(+route.paramMap.get('id')!);
};
