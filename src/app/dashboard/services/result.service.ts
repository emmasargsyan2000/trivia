import { Injectable } from '@angular/core';
import {TestResult} from "../../quiz/models/question";

@Injectable()
export class ResultService {

  private STORAGE_KEY = "test_result"

  public saveTestResult(testResult: TestResult) {
    let testResults = this.getTestsResult() ;
    testResult.index = testResults.length + 1;
    testResults.push(testResult)
    localStorage.setItem(this.STORAGE_KEY, JSON.stringify(testResults));
  }

  public getTestsResult(): TestResult[] {
    return JSON.parse(localStorage.getItem(this.STORAGE_KEY) || "[]");
  }

  public getTestResult(testId: number): TestResult | undefined {
    return this.getTestsResult().find(result => result.index === testId);
  }


}
