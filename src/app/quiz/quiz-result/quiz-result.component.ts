import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Category} from "../models/category";

@Component({
  selector: 'app-quiz-result',
  templateUrl: './quiz-result.component.html',
  styleUrls: ['./quiz-result.component.scss']
})
export class QuizResultComponent {

  @Input() total!: number;
  @Input() scored!: number;
  @Output() backEmitter: EventEmitter<void> = new EventEmitter<void>();

  public backToHome(){
    this.backEmitter.emit()
  }

}
