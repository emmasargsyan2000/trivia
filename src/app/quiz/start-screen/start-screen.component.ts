import {Component, EventEmitter, inject, OnInit, Output} from '@angular/core';
import {Category} from "../models/category";
import {CategoryService} from "../services/category.service";
import {first} from "rxjs";

@Component({
  selector: 'app-start-screen',
  templateUrl: './start-screen.component.html',
  styleUrls: ['./start-screen.component.scss']
})
export class StartScreenComponent implements OnInit {

  public categories: Category[] = []
  public selectedCategory!: number;
  @Output() categorySelect: EventEmitter<Category> = new EventEmitter<Category>();
  private categoryService: CategoryService = inject(CategoryService);

  ngOnInit(): void {
    this.categoryService.loadCategories().pipe(first()).subscribe(categories => this.categories = categories)
  }

  startClick(): void{
    this.categorySelect.emit(this.categories.find(category => category.value === this.selectedCategory))
  }

}
