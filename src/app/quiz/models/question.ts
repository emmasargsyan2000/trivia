export interface Question {
  question: string,
  correct_answer: string,
  incorrect_answers: string[],
  category: string,
  type: QuestionType,
  difficulty: DifficultyType,
  index?: number,
  selected_answer?: string
}
export interface QuestionResponse {
  results: Question[],
  response_code: number
}

export interface TestResult {
  questions: Question[],
  scored: number,
  category: string,
  index?: number
}

export type QuestionType = "boolean" | "multiple";
export type DifficultyType = "easy" | "medium" | "hard";

