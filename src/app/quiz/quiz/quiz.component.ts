import {Component, inject} from '@angular/core';
import {first} from "rxjs";
import {Category} from "../models/category";
import {QuizService} from "../services/quiz.service";
import {Question} from "../models/question";
import {ResultService} from "../../dashboard/services/result.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent {

  public selectedCategory: Category | undefined;
  public questions: Question[] = []
  public scored: number = 0;
  public currentQuestionIndex!: number;

  private quizService: QuizService = inject(QuizService);
  private resultService: ResultService = inject(ResultService);

  public selectCategory(category: Category){
    this.selectedCategory = category;
    this.quizService.loadQuestions(this.selectedCategory.value).pipe(first())
      .subscribe(questions => {
        this.questions = questions;
        this.currentQuestionIndex = 0;
      })
  }

  public startAgain(){
    this.selectedCategory = undefined;
    this.questions = []
    this.scored = 0
    this.currentQuestionIndex = -1
  }

  public selectAnswer(selectedAnswer: string){
    this.questions[this.currentQuestionIndex].selected_answer = selectedAnswer
    if(this.questions[this.currentQuestionIndex].correct_answer == selectedAnswer)
      this.scored++;
    this.currentQuestionIndex++
    if( this.currentQuestionIndex === this.questions.length)
      this.saveResult();
  }

  private saveResult(){
    this.resultService.saveTestResult({
      questions: this.questions,
      scored: this.scored,
      category: this.selectedCategory?.name || ""
    })
  }

}
