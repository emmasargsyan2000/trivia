import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Question} from "../models/question";

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnChanges {

  @Input()
  public question!: Question;

  @Output() selectAnswerEmitter: EventEmitter<string> = new EventEmitter<string>();

  public answers: string[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    if(changes && changes["question"]) {
      this.answers = []
      this.answers = this.answers.concat(this.question.correct_answer).concat(...this.question.incorrect_answers);
      this.answers = this.answers.map(value => ({value, sort: Math.random()}))
        .sort((a, b) => a.sort - b.sort)
        .map(({value}) => value)
    }
  }

  selectAnswer(selectedAnswer: string){
    this.selectAnswerEmitter.emit(selectedAnswer);
  }

}
