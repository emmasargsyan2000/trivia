import {inject, Injectable} from '@angular/core';
import {map, Observable} from "rxjs";
import {Category} from "../models/category";
import {HttpClient} from "@angular/common/http";
import {Question, QuestionResponse} from "../models/question";

@Injectable()
export class QuizService {

  private URL: string = "https://opentdb.com/api.php"
  private http: HttpClient = inject(HttpClient);

  public loadQuestions(category: number, amount: number = 10): Observable<Question[]>{
    return this.http.get<QuestionResponse>(this.URL + `?amount=${amount}&category=${category}`).pipe(
      map(questions => questions.results.map( (question, index) => {
          question.index = index + 1;
          return question;
        })
      )
    );
  }

}
