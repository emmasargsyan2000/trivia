import {inject, Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Category} from "../models/category";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class CategoryService {

  private http: HttpClient = inject(HttpClient);

  public loadCategories(): Observable<Category[]>{
    return this.http.get<Category[]>("/assets/categories.json");
  }

}
