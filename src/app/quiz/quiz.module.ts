import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizComponent } from './quiz/quiz.component';
import {QuestionComponent} from "./question/question.component";
import {QuizRoutingModule} from "./quiz-routing.module";
import {ResultService} from "../dashboard/services/result.service";
import {QuizService} from "./services/quiz.service";
import {MatFormField, MatFormFieldModule, MatLabel} from "@angular/material/form-field";
import {StartScreenComponent} from "./start-screen/start-screen.component";
import {QuizResultComponent} from "./quiz-result/quiz-result.component";
import {MatSelectModule} from "@angular/material/select";
import {CategoryService} from "./services/category.service";
import {MatButtonModule} from "@angular/material/button";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    QuizComponent,
    QuestionComponent,
    StartScreenComponent,
    QuizResultComponent
  ],
  imports: [
    CommonModule,
    QuizRoutingModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    HttpClientModule
  ],
  providers: [
    ResultService,
    QuizService,
    CategoryService
  ]
})
export class QuizModule { }
